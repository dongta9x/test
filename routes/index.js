var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: {test : 'Express', test1 : 'test1'} });
});

router.get('/list', function(req, res, next) {
  let query = "select * from players";
  db.query(query, function(err, result){
    if(err){
      res.redirect('/');
    }
    res.render('list', {
      title : "LIST",
      players : result
    })
  })
});

router.get('/add', function(req, res, next) {
  let query = "insert into players(first_name, last_name, position, number, image, user_name) values ?";
  let values = [
    ['abc','abc','abc',1,'abc','abc'],
    ['abc','abc','abc',1,'abc','abc']
  ]
  db.query(query, [values], function(err, result){
    if(err){
      res.redirect('/');
    }
    console.log(result);
    res.render('add', {
      title : "ADD",
      players : result
    })
  })
});
module.exports = router;
